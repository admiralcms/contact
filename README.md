# DEPRECATED
Due to the fairly easy to use `Email` class present in the Admiral core, this plugin is now redundant for building contact forms.

# Contact
Plugin for creating and handling contact forms with Admiral
