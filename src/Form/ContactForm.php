<?php
  namespace Admiral\Contact\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class ContactForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('name', ['type' => 'string'])
        ->addField('email', ['type' => 'string'])
        ->addField('subject', ['type' => 'string'])
        ->addField('message', ['type' => 'textarea']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the Name field
      ->requirePresence('name')
      ->notEmpty('name', 'Please enter your name')
      // Rules for the Email field
      ->add('email', 'format', [
        'rule' => 'email',
        'message' => 'Please enter a valid email',
      ])
      // Rules for the Subject field
      ->requirePresence('subject')
      ->notEmpty('subject', 'Please enter a subject')
      // Rules for the Message field
      ->requirePresence('message')
      ->notEmpty('message', 'Please write your message');
      return $validator;
    }
  }
