<?php
  namespace Admiral\Contact\Controller;

  use Admiral\Contact\Controller\AppController;
  use Admiral\Contact\Form\ContactForm;
  use Admiral\Admiral\Email;
  use Cake\Core\Configure;

  class ContactController extends AppController {
    public function initialize(){
      parent::initialize();
    }

    public function index() {
      $contactForm = new ContactForm();
      if ($this->request->is('post')) {
        if ($contactForm->validate($this->request->getData())) {
          // Build our mail
          $email = new Email();
          $email->set('to', Configure::read('Contact.default.to'));
          $email->set('subject', $this->request->getData('subject'));
          $email->set('from', [
            'mail' => $this->request->getData('email'),
            'name' => $this->request->getData('name'),
          ]);
          $email->set('viewVars', [
            'content' => $this->request->getData('message'),
          ]);
  
          // Try to send the mail
          if(!$email->send()) {
            $this->Flash->error('There was an issue sending your mail. Please try again later!');
          } else {
            $this->Flash->success('We will get back to you asap!');
          }
        } else {
          $this->Flash->error('Please fill out the contact form properly');
        }
      }
      $this->set('contactForm', $contactForm);
    }
  }
