<?php
  namespace Admiral\Contact\Controller;

  use App\Controller\AppController as BaseController;
  use Cake\Core\Configure;

  class AppController extends BaseController {
    public function initialize() {
      parent::initialize();
      $this->Auth->allow(['index']);
    }
  }
